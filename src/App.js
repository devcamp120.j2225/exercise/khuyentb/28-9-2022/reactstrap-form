import "bootstrap/dist/css/bootstrap.min.css"
import ReactstrapForm from "./Components/ReactstrapForm";

function App() {
  return (
    <div className="App">
      <ReactstrapForm></ReactstrapForm>
    </div>
  );
}

export default App;
