import { Button, Col, Input, Label, Row } from "reactstrap";
import image from "../asset/image/LISA.jpeg"

function ReactstrapForm() {
    return (
        <div className="container">
            <Row className="text-center mt-2">
                <h3>EMPLOYEE PROFILE</h3>
            </Row>
            <Row>
                <Col sm={8}>
                    <Row className="mt-3">
                        <Col sm={3}>
                            <Label>Employee Name</Label>
                        </Col>
                        <Col sm={9}>
                            <Input placeholder="Please enter your name"></Input>
                        </Col>
                    </Row>
                    <Row className="mt-3">
                        <Col sm={3}>
                            <Label>Day of Birth</Label>
                        </Col>
                        <Col sm={9}>
                            <Input placeholder="Please enter your DOB"></Input>
                        </Col>
                    </Row>
                    <Row className="mt-3">
                        <Col sm={3}>
                            <Label>Phone Number</Label>
                        </Col>
                        <Col sm={9}>
                            <Input placeholder="Please enter your phone number"></Input>
                        </Col>
                    </Row>
                    <Row className="mt-3">
                        <Col sm={3}>
                            <Label>Gender</Label>
                        </Col>
                        <Col sm={9}>
                            <Input placeholder="Please enter your gender"></Input>
                        </Col>
                    </Row>
                </Col>
                <Col sm={4} className="text-center" style={{ backgroundColor: "lightgrey" }}>
                    <img src={image} style={{ width: "300px", height: "220px" }}></img>
                </Col>
            </Row >
            <Row className="mt-3">
                <Col sm={2}>
                    <Label>Work</Label>
                </Col>
                <Col sm={10}>
                    <Input placeholder="Please enter your work"></Input>
                </Col>
            </Row>
            <Row className="mt-3">
                <Col>
                    <Button style={{float: "right"}} color="success">Detail</Button>
                    <Button style={{float: "right", marginRight: "3px"}} color="success">Check</Button>
                </Col>
            </Row>
        </div>
    )
}

export default ReactstrapForm